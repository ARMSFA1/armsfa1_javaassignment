package pBurritoDataSet;
//base class of collection
public class Burrito {
	private String location;
	private String burrito;
	private Double cost;
	private Double hunger;
	private Double tortilla;
	private Double fillings;
	private Double salsa;
	private Double synergy;
	private Double overall;
	private String reviewer;
	
	public Burrito(String location, String burrito, Double cost, Double hunger, Double tortilla, Double fillings, Double salsa, Double synergy, Double overall, String reviewer)
	{
		//initializing constructor
		this.setLocation(location);
		this.setBurrito(burrito);
		this.setCost(cost);
		this.setHunger(hunger);
		this.setTortilla(tortilla);
		this.setFillings(fillings);
		this.setSalsa(salsa);
		this.setSynergy(synergy);
		this.setOverall(overall);
		this.setReviewer(reviewer);
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public Double getHunger() {
		return hunger;
	}

	public void setHunger(Double hunger) {
		this.hunger = hunger;
	}

	public Double getTortilla() {
		return tortilla;
	}

	public void setTortilla(Double tortilla) {
		this.tortilla = tortilla;
	}

	public Double getFillings() {
		return fillings;
	}

	public void setFillings(Double filling) {
		this.fillings = filling;
	}

	public Double getSalsa() {
		return salsa;
	}

	public void setSalsa(Double salsa) {
		this.salsa = salsa;
	}

	public Double getSynergy() {
		return synergy;
	}

	public void setSynergy(Double synergy) {
		this.synergy = synergy;
	}

	public Double getOverall() {
		return overall;
	}

	public void setOverall(Double overall) {
		this.overall = overall;
	}

	public String getReviewer() {
		return reviewer;
	}

	public void setReviewer(String reviewer) {
		this.reviewer = reviewer;
	}

	public String getBurrito() {
		return burrito;
	}

	public void setBurrito(String burrito) {
		this.burrito = burrito;
	}
	

}
