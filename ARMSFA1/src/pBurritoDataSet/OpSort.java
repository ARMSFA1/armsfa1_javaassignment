package pBurritoDataSet;
//enum for combo box
public enum OpSort {
	RESULT("Result"),
	COST("Cost"),
	HUNGER("Hunger"),
	TORTILLA("Tortilla"),
	FILLINGS("Fillings"),
	SALSA("Salsa"),
	SYNERGY("Synergy"),
	OVERALL("Overall"),
	LOCATION("Location"),
	REVIEWER("Reviewer"),
	BURRITO("Burrito");
	private final String name;
	
	OpSort(String name)
	{
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
