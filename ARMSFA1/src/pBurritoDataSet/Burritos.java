package pBurritoDataSet;
import java.util.ArrayList;

import pBurritoDataSet.Burrito;
public class Burritos {
	private ArrayList <Burrito> burritoList = new ArrayList<Burrito>();
	private String collectionName;
	
	public Burritos(String collectionName)
	{
		this.setCollectionName(collectionName);
	}
	
	
	public ArrayList<Burrito> populate(ArrayList<String> list)
	{
		String delimiter = "  ";
		ArrayList <Burrito> burritoList = new ArrayList<Burrito>();
		for(String line : list)
		{
			String[] tempLine = line.split(delimiter);
			for(int i =0; i < tempLine.length; i++)
			{
				if(tempLine[i].contains("n/a"))
				{
					tempLine[i] = "0.0";
				}
			} //populating ArrayList with burrito objects converting type where appropriate
			burritoList.add(new Burrito(tempLine[0], tempLine[1], Double.parseDouble(tempLine[2]), Double.parseDouble(tempLine[3]), Double.parseDouble(tempLine[4]), Double.parseDouble(tempLine[5]), 
									Double.parseDouble(tempLine[6]), Double.parseDouble(tempLine[7]), Double.parseDouble(tempLine[8]), tempLine[9]));
			
		}
		
		return burritoList;
		
	}
	public ArrayList<Burrito> getBurritoList()
	{
		return burritoList;
	}
	
	public String getCollectionName() {
		return collectionName;
	}
	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}	
}
