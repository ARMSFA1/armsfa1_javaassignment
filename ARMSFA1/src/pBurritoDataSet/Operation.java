package pBurritoDataSet;
//enum for combo box
public enum Operation {
	
	ASCENDING("Ascending"),
	DESCENDING("Descending");
	private final String order;
	
	Operation(String order)
	{
		this.order = order;
	}

	public String getOrder() {
		return order;
	}

}
