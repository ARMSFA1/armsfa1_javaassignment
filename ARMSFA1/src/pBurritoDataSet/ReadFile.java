package pBurritoDataSet;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class ReadFile{

	private File file;
	private Scanner scan;
	private ArrayList <String> list = new ArrayList <String> (); 
	//originally read file to string arraylist because it could of been useful.
	public ReadFile()
	{
		
	}
	public File getFile()
	{
		return file;
		
	}
	
	public void setFile(File file)
	{
		this.file=file;
	}
		
	public ArrayList<String> read(String filePath)
	{
		// count lines
		//create array and copy elements
		
		
		file = new File (filePath);
		try {
			scan = new Scanner(file);
			while (scan.hasNextLine())
			{			
				
				list.add(scan.nextLine());
			}
				scan.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	public ArrayList<String> getLines() { //provides access to array list data
		return list;
	}

	public void setLines(ArrayList<String> lines) {
		this.list = lines;
	}
	


}
