package pBurritoDataSet;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.Dimension;
import javax.swing.JTabbedPane;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;
import java.awt.Color;
import javax.swing.ImageIcon;

public class GUI extends JFrame {
	
	//for passing collection to GUI
	private static ArrayList<Burrito> BurritoList = new ArrayList<Burrito>();
	private JPanel contentPane;
	private JTable table_1;
	//instance of enums for combo boxes
	private static Operation opnum;
	private static OpSort sortnum;
	private static int x;
	//instance of table model (hard coded columns rows passed later
	private DefaultTableModel dtm = new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Location", "Burrito", "Cost", "Hunger", "Tortilla", "Fillings", "Salsa", "Synergy", "Overall", "Reviewer"
			}
		);
	private JTextField locationField;
	private JTextField burritoField;
	private JTextField costField;
	private JTextField hungerField;
	private JTextField tortillaField;
	private JTextField fillingsField;
	private JTextField salsaField;
	private JTextField synergyField;
	private JTextField overallField;
	private JTextField reviewerField;
	private JTextField calcTextField;

	public GUI(ArrayList<Burrito> dataSet) {
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\GGPC\\Downloads\\ARMSFA1\\ARMSFA1\\burrito.png")); //setting app icon
		setTitle("San Diego Burritos");
		this.BurritoList = dataSet;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1100, 500);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 153, 51));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 11, 1064, 439);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(153, 204, 51));
		panel.setSize(new Dimension(600, 0));
		panel.setMinimumSize(new Dimension(600, 10));
		panel.setPreferredSize(new Dimension(600, 10));
		tabbedPane.addTab("Register", null, panel, null);
		panel.setLayout(null);
		JScrollPane resultPanel = new JScrollPane();
		resultPanel.setBounds(0, 0, 893, 411);
		panel.add(resultPanel);
		
		//table formatting
		table_1 = new JTable();
		table_1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table_1.setEnabled(false);
		table_1.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table_1.setModel(dtm);
		table_1.getColumnModel().getColumn(9).setPreferredWidth(120);
		table_1.getColumnModel().getColumn(0).setPreferredWidth(250);
		table_1.getColumnModel().getColumn(1).setPreferredWidth(220);
		recreate();
		resultPanel.setViewportView(table_1);
		
		JComboBox operation = new JComboBox(Operation.values()); //grabs values ASCENDING/DESCENDING for combo box
		operation.setBounds(903, 27, 146, 22);
		panel.add(operation);
		
		JComboBox opOrder = new JComboBox(OpSort.values()); //grabs sortable values from enum for combo box
		opOrder.setBounds(903, 60, 146, 22);
		panel.add(opOrder);
		
		JButton btnSort = new JButton("Sort");
		btnSort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String sortValue = operation.getSelectedItem().toString(); //converts combobox values to string for checking against switch
				String value = opOrder.getSelectedItem().toString();
				sortButton(sortValue, value);
				
		}});
		btnSort.setBounds(903, 93, 89, 23);
		panel.add(btnSort);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnExit.setBounds(960, 377, 89, 23);
		panel.add(btnExit);
		
		
		//calculation comboboxes
		JComboBox calcMethod = new JComboBox();
		calcMethod.setModel(new DefaultComboBoxModel(new String[] {"MEAN", "MODE", "MEDIAN"}));
		calcMethod.setBounds(903, 182, 85, 20);
		panel.add(calcMethod);
		
		JComboBox calcVariable = new JComboBox();
		calcVariable.setModel(new DefaultComboBoxModel(new String[] {"COST", "FILLINGS", "HUNGER", "SALSA", "SYNERGY", "TORTILLA", "OVERALL"}));
		calcVariable.setBounds(903, 213, 85, 20);
		panel.add(calcVariable);
		
		JButton btnCalculate = new JButton("Calculate");
		btnCalculate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String sortValue2 = calcMethod.getSelectedItem().toString();
				String value2 = calcVariable.getSelectedItem().toString();
				calculateButton(sortValue2, value2);
				
			}
		});
		btnCalculate.setBounds(903, 244, 89, 23);
		panel.add(btnCalculate);
		
		calcTextField = new JTextField();
		calcTextField.setBounds(903, 278, 86, 20);
		panel.add(calcTextField);
		calcTextField.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(153, 102, 51));
		tabbedPane.addTab("Record", null, panel_1, null);
		panel_1.setLayout(null);
		
		JButton btnFirst = new JButton("First");
		btnFirst.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				createRecord(0);
				
			}
		});
		btnFirst.setBounds(24, 293, 89, 23);
		panel_1.add(btnFirst);
		
		JButton btnLast = new JButton("Last");
		btnLast.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				createRecord(1419);
			}
		});
		btnLast.setBounds(339, 293, 89, 23);
		panel_1.add(btnLast);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(10, 11, 431, 316);
		panel_1.add(panel_2);
		panel_2.setLayout(null);
		
		
		//labels and buttons for record tab
		JButton btnPrevious = new JButton("<< Previous");
		btnPrevious.setBounds(108, 282, 89, 23);
		panel_2.add(btnPrevious);
		
		JButton btnNext = new JButton("Next >>");
		btnNext.setBounds(232, 282, 89, 23);
		panel_2.add(btnNext);
		
		JLabel lblNewLabel_1 = new JLabel("Burrito Type");
		lblNewLabel_1.setBounds(232, 11, 100, 14);
		panel_2.add(lblNewLabel_1);
		
		JLabel lblNewLabel_3 = new JLabel("Hunger");
		lblNewLabel_3.setBounds(232, 59, 100, 14);
		panel_2.add(lblNewLabel_3);
		
		JLabel lblNewLabel_5 = new JLabel("Fillings");
		lblNewLabel_5.setBounds(232, 110, 100, 14);
		panel_2.add(lblNewLabel_5);
		
		JLabel lblNewLabel_7 = new JLabel("Overall");
		lblNewLabel_7.setBounds(232, 154, 46, 14);
		panel_2.add(lblNewLabel_7);
		
		JLabel lblNewLabel_8 = new JLabel("Reviewer");
		lblNewLabel_8.setBounds(232, 205, 68, 14);
		panel_2.add(lblNewLabel_8);
		
		burritoField = new JTextField();
		burritoField.setBounds(232, 23, 153, 20);
		panel_2.add(burritoField);
		burritoField.setColumns(10);
		
		hungerField = new JTextField();
		hungerField.setBounds(232, 73, 153, 20);
		panel_2.add(hungerField);
		hungerField.setColumns(10);
		
		fillingsField = new JTextField();
		fillingsField.setBounds(232, 123, 153, 20);
		panel_2.add(fillingsField);
		fillingsField.setColumns(10);
		
		overallField = new JTextField();
		overallField.setBounds(232, 167, 153, 20);
		panel_2.add(overallField);
		overallField.setColumns(10);
		
		reviewerField = new JTextField();
		reviewerField.setBounds(232, 218, 153, 20);
		panel_2.add(reviewerField);
		reviewerField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Location");
		lblNewLabel.setBounds(57, 11, 100, 14);
		panel_2.add(lblNewLabel);
		
		JLabel lblNewLabel_2 = new JLabel("Cost");
		lblNewLabel_2.setBounds(54, 59, 100, 14);
		panel_2.add(lblNewLabel_2);
		
		JLabel lblNewLabel_4 = new JLabel("Tortilla");
		lblNewLabel_4.setBounds(55, 110, 100, 14);
		panel_2.add(lblNewLabel_4);
		
		JLabel lblNewLabel_6 = new JLabel("Salsa");
		lblNewLabel_6.setBounds(57, 154, 100, 14);
		panel_2.add(lblNewLabel_6);
		
		JLabel lblNewLabel_9 = new JLabel("Synergy");
		lblNewLabel_9.setBounds(57, 205, 100, 14);
		panel_2.add(lblNewLabel_9);
		
		locationField = new JTextField();
		locationField.setBounds(57, 23, 117, 20);
		panel_2.add(locationField);
		locationField.setColumns(10);
		
		costField = new JTextField();
		costField.setBounds(56, 73, 118, 20);
		panel_2.add(costField);
		costField.setColumns(10);
		
		tortillaField = new JTextField();
		tortillaField.setBounds(56, 123, 118, 20);
		panel_2.add(tortillaField);
		tortillaField.setColumns(10);
		
		salsaField = new JTextField();
		salsaField.setBounds(56, 167, 118, 20);
		panel_2.add(salsaField);
		salsaField.setColumns(10);
		
		synergyField = new JTextField();
		synergyField.setBounds(56, 218, 118, 20);
		panel_2.add(synergyField);
		synergyField.setColumns(10);
		
		
		//record tab background
		JLabel lblNewLabel_10 = new JLabel("New label");
		lblNewLabel_10.setIcon(new ImageIcon("F:\\java\\Java_Assignment\\burrito.jpg"));
		lblNewLabel_10.setBounds(-87, -52, 1167, 477);
		panel_1.add(lblNewLabel_10);
		
		//next button
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(x < BurritoList.size()-1)
				{
					
					x++;
					createRecord(x);
					
				}
				
			}
		});
		
		//previous button
		btnPrevious.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(x > 0)
				{
					x--;
					createRecord(x);
				}
			}
		});
		
		createRecord(0);
	
	}
	
	public void recreate()
	{
		
		//populate table with rows looping through collection of objects
		dtm.setRowCount(0);
		for(int i = 0; i < BurritoList.size(); i++)
		{
			Object[] obj = new Object[10];
			obj[0] = BurritoList.get(i).getLocation();
			obj[1] = BurritoList.get(i).getBurrito();
			obj[2] = BurritoList.get(i).getCost();
			obj[3] = BurritoList.get(i).getHunger();
			obj[4] = BurritoList.get(i).getTortilla();
			obj[5] = BurritoList.get(i).getFillings();
			obj[6] = BurritoList.get(i).getSalsa();
			obj[7] = BurritoList.get(i).getSynergy();
			obj[8] = BurritoList.get(i).getOverall();
			obj[9] = BurritoList.get(i).getReviewer();
			dtm.addRow(obj);
		}
	}
	
	public void createRecord(int x)
	{
		//populate text fields in record tab with data
		locationField.setText(BurritoList.get(x).getLocation());
		burritoField.setText(BurritoList.get(x).getBurrito());
		costField.setText(Double.toString(BurritoList.get(x).getCost()));
		hungerField.setText(Double.toString(BurritoList.get(x).getHunger()));
		tortillaField.setText(Double.toString(BurritoList.get(x).getTortilla()));
		fillingsField.setText(Double.toString(BurritoList.get(x).getFillings()));
		salsaField.setText(Double.toString(BurritoList.get(x).getSalsa()));
		synergyField.setText(Double.toString(BurritoList.get(x).getSynergy()));
		overallField.setText(Double.toString(BurritoList.get(x).getOverall()));
		reviewerField.setText(BurritoList.get(x).getReviewer());
	}
	public void sortButton(String sortValue,String value)
	{
		switch(value)
		{
		case "LOCATION":
			sortByLocation sbl = new sortByLocation(); //instantiating comparator class
			Collections.sort(BurritoList, sbl);       //sorting collection ascending
			if(sortValue == "ASCENDING")
			{
				recreate();						//if ascending is selected in combo box then repopulate table with sorted collection
			}
			else
			{
				Collections.reverse(BurritoList); // else reverse the order (descending and re-populate table)
				recreate();
			}
			break;
		case "BURRITO":
			sortByBurrito sbb = new sortByBurrito();
			Collections.sort(BurritoList, sbb);
		if(sortValue == "ASCENDING")
		{
			
			recreate();
		}
		else
		{
			Collections.reverse(BurritoList);
			recreate();
		}
		break;
		case "REVIEWER":
			sortByReviewer sbr = new sortByReviewer();
			Collections.sort(BurritoList, sbr);
		if(sortValue == "ASCENDING")
		{
			
			recreate();
		}
		else
		{
			Collections.reverse(BurritoList);
			recreate();
		}
		break;
		case "HUNGER":
			sortByLocation sbh = new sortByLocation();
			Collections.sort(BurritoList, sbh);
		if(sortValue == "ASCENDING")
		{
			
			recreate();
		}
		else
		{
			Collections.reverse(BurritoList);
			recreate();
			
		}
		break;
		case "TORTILLA":
			sortByTortilla sbt = new sortByTortilla();
			Collections.sort(BurritoList, sbt);
		if(sortValue == "ASCENDING")
		{
			recreate();
		}
		else
		{
			Collections.reverse(BurritoList);
			recreate();
		}
		break;
		case "FILLINGS":
			sortByFillings sbf = new sortByFillings();
			Collections.sort(BurritoList, sbf);
		if(sortValue == "ASCENDING")
		{
			recreate();
		}
		else
		{
			Collections.reverse(BurritoList);
			recreate();
		}
		break;
		case "SALSA":
			sortBySalsa sbs = new sortBySalsa();
			Collections.sort(BurritoList, sbs);
		if(sortValue == "ASCENDING")
		{
			recreate();
		}
		else
		{
			Collections.reverse(BurritoList);
			recreate();
		}
		break;
		case "SYNERGY":
			sortBySynergy sbS = new sortBySynergy();
			Collections.sort(BurritoList, sbS);
		if(sortValue == "ASCENDING")
		{
			recreate();
		}
		else
		{
			Collections.reverse(BurritoList);
			recreate();
		}
		break;
		case "OVERALL":
			sortByOverall sbo = new sortByOverall();
			Collections.sort(BurritoList, sbo);
		if(sortValue == "ASCENDING")
		{
			recreate();
		}
		else
		{
			Collections.reverse(BurritoList);
			recreate();
		}
		break;
		}
		
}
	
	public void calculateButton(String sortValue2,String value2)
	{
		sortByCost sbc2 = new sortByCost();
		sortByHunger sbh2 = new sortByHunger();
		sortByTortilla sbt2 = new sortByTortilla();
		sortByFillings sbf2 = new sortByFillings();
		sortBySalsa sbs2 = new sortBySalsa();
		sortBySynergy sbS2 = new sortBySynergy();
		sortByOverall sbo2 = new sortByOverall();
		//sorting all collections before performing calculations
		if(sortValue2 == "COST")
		{
			Collections.sort(BurritoList, sbc2);
		}
		
		else if(sortValue2 == "HUNGER")
		{
			Collections.sort(BurritoList, sbh2);
		}
		
		else if(sortValue2 == "TORTILLA")
		{
			Collections.sort(BurritoList, sbt2);
		}

		else if(sortValue2 == "FILLINGS")
		{
			Collections.sort(BurritoList, sbf2);
		}

		else if(sortValue2 == "SALSA")
		{
			Collections.sort(BurritoList, sbs2);
		}

		else if(sortValue2 == "SYNERGY")
		{
			Collections.sort(BurritoList, sbS2);
		}

		else if(sortValue2 == "OVERALL")
		{
			Collections.sort(BurritoList, sbo2);
		}
		
		switch(sortValue2) {
		case "MEAN":
				calcTextField.setText(Utilities.findMean(BurritoList, value2));
			break;
		case "MODE":
				calcTextField.setText(Utilities.findMode(BurritoList, value2));
			break;
		case "MEDIAN":
				calcTextField.setText(Utilities.findMedian(BurritoList, value2));
			break;
		}
	}
}

