package pBurritoDataSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;

public class Utilities{
	//calculation methods
	public static String findMean(ArrayList <Burrito> burritoList, String value2)
	{
		//formatting to two decimal places
		DecimalFormat dec = new DecimalFormat("0.##");
		
		double output = 0.0;
		
		for(int i = 0; i < burritoList.size(); i++)
		{
			if(value2 == "COST" && burritoList.get(i).getCost() != 0.0)
			{
				output = output + burritoList.get(i).getCost();
			}
			else if(value2 == "HUNGER" && burritoList.get(i).getHunger() != 0.0)
			{
				output = output + burritoList.get(i).getHunger();
			}
			else if(value2 == "TORTILLA" && burritoList.get(i).getTortilla() != 0.0)
			{
				output = output + burritoList.get(i).getTortilla();
			}
			else if(value2 == "FILLINGS" && burritoList.get(i).getFillings() != 0.0)
			{
				output = output + burritoList.get(i).getFillings();
			}
			else if(value2 == "SALSA" && burritoList.get(i).getSalsa() != 0.0)
			{
				output = output + burritoList.get(i).getSalsa();
			}
			else if(value2 == "SYNERGY" && burritoList.get(i).getSynergy() != 0.0)
			{
				output = output + burritoList.get(i).getSynergy();
			}
			else if(value2 == "OVERALL" && burritoList.get(i).getOverall() != 0.0)
			{
				output = output + burritoList.get(i).getOverall();
			}
			
		}
		
		output = output / burritoList.size();
		
		return dec.format(output);
	}
	
	public static String findMedian(ArrayList <Burrito> burritoList, String value2)
	{
		// There are 405 numbers. 405 + 1 = 406. 406/2 = 203
		if(value2 == "COST")
		{
			Double mn = (burritoList.get(203).getCost() + burritoList.get(203).getCost()) / 2;
			return mn.toString();
		}
		
		if(value2 == "HUNGER")
		{
			Double mn = (burritoList.get(203).getHunger() + burritoList.get(203).getHunger()) / 2;
			return mn.toString();
		}
		
		if(value2 == "TORTILLA")
		{
			Double mn = (burritoList.get(203).getTortilla() + burritoList.get(203).getTortilla()) / 2;
			return mn.toString();
		}
		
		if(value2 == "FILLINGS")
		{
			Double mn = (burritoList.get(203).getFillings() + burritoList.get(203).getFillings()) / 2;
			return mn.toString();
		}
		
		if(value2 == "SALSA")
		{
			Double mn = (burritoList.get(203).getSalsa() + burritoList.get(203).getSalsa()) / 2;
			return mn.toString();
		}
		
		if(value2 == "SYNERGY")
		{
			Double mn = (burritoList.get(203).getSynergy() + burritoList.get(203).getSynergy()) / 2;
			return mn.toString();
		}
		
		if(value2 == "OVERALL")
		{
			Double mn = (burritoList.get(203).getOverall() + burritoList.get(203).getOverall()) / 2;
			return mn.toString();
		}
		return "";
		
	}

	public static String findMode(ArrayList <Burrito> burritoList, String value2)
	{
		DecimalFormat dec = new DecimalFormat("0.##");
		double modeVal = 0; 
		double maxVal = 0;
		
		for(int i = 0; i < burritoList.size(); ++i)
		{
			int x = 0;
			for(int y = 0; y < burritoList.size(); ++y)
			{
					if(value2 == "COST" && burritoList.get(y).getCost() == burritoList.get(i).getCost())
					{
						++x;
					}


					if(value2 == "HUNGER" && burritoList.get(y).getCost() == burritoList.get(i).getCost())
					{
						++x;
					}


					if(value2 == "TORTILLA" && burritoList.get(y).getTortilla() == burritoList.get(i).getTortilla())
					{
						++x;
					}
					
					if(value2 == "FILLINGS" && burritoList.get(y).getFillings() == burritoList.get(i).getFillings())
					{
						++x;
					}
				
					if(value2 == "SALSA" && burritoList.get(y).getSalsa() == burritoList.get(i).getSalsa())
					{
						++x;
					}
				
					if(value2 == "SYNERGY" && burritoList.get(y).getSynergy() == burritoList.get(i).getSynergy())
					{
						++x;
					}
				
					if(value2 == "OVERALL" && burritoList.get(y).getOverall() == burritoList.get(i).getOverall())
					{
						++x;
					}
				
			}
			
			if(x > maxVal)
			{
				
				
				if(value2 == "COST")
				{
					maxVal = x;
					modeVal = burritoList.get(i).getCost();
				}
				
				else if(value2 == "HUNGER")
				{
					maxVal = x;
					modeVal = burritoList.get(i).getHunger();
				}
				else if(value2 == "TORTILLA")
				{
					maxVal = x;
					modeVal = burritoList.get(i).getTortilla();
				}
				else if(value2 == "FILLINGS")
				{
					maxVal = x;
					modeVal = burritoList.get(i).getFillings();
				}
				else if(value2 == "SALSA")
				{
					maxVal = x;
					modeVal = burritoList.get(i).getSalsa();
				}
				else if(value2 == "SYNERGY")
				{
					maxVal = x;
					modeVal = burritoList.get(i).getSynergy();
				}
				else if(value2 == "OVERALL")
				{
					maxVal = x;
					modeVal = burritoList.get(i).getOverall();
				}
			}
			
			
			
		}
		return dec.format(modeVal);
		
	}
}
