/* Created by Finn Armstrong-McAllister
 * Java, BIT Otago Polytechnic
 * Assignment 1
 * Finished 4/11/2019
 * The purpose of this program is to read a list of data from a text file and then implement into into a dataset, 
 * which the user is able to interact with via a GUI
 */

 package pBurritoDataSet;
 
import java.io.*;
public class App
{
	public static void main(String[] args) throws FileNotFoundException 
	{
	//instance of collection class
	Burritos dataSet = new Burritos("San Diego Burrito's");
	//instantiating reader
	ReadFile read = new ReadFile();
	//pass filepath to readfile method
	read.read("burrito.txt");
	//load gui
	GUI frame = new GUI(dataSet.populate((read.getLines())));
	frame.setVisible(true);
	
	}
	

}
	
